const util = require("../../util");
var request = require("request");
var qs = require("querystring");
var chalk = require("chalk");
const { MessageEmbed } = require("discord.js");

function get_gif(tags, callback) {
  var query = qs.stringify({
    api_key: "wlK1lkVeu137oH45AZ5vD52iSu3XjTB8",
    rating: "r",
    format: "json",
    limit: 1,
  });

  if (tags) {
    query += "&tag=" + tags.join("+");
  }

  request(
    `http://api.giphy.com/v1/gifs/random?${query}`,
    (error, response, body) => {
      if (error || response.statusCode !== 200) {
        util.logger.error(chalk.red(`Giphy Got Error: ${body.trim()}`));
      } else {
        try {
          callback(
            `http://media.giphy.com/media/${JSON.parse(body).data.id}/giphy.gif`
          );
        } catch (err) {
          util.logger.error(err);
          callback(undefined);
        }
      }
    }
  );
}

function getTagsString(args) {
  return args.length > 0 ? `Search Tags: [${args}]` : "";
}

module.exports = {
  commands: {
    giphy: {
      name: "giphy",
      help: "Display a random GIF from Giphy",
      parameters: {
        params: ["[tags]"],
      },
      main: (bot, db, msg) => {
        var { args } = util.args.parse(msg);

        get_gif(args, (gif) => {
          if (gif !== undefined) {
            return msg.channel.send(
              new MessageEmbed().setImage(gif).setFooter(getTagsString(args))
            );
          }
          return msg.channel.send(
            "Giphy Rejected Args, Please Try new ones. " + getTagsString(args)
          );
        });
      },
    },
  },
  events: {},
};
